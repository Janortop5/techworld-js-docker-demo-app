def gv

pipeline {   
    agent any

    environment {
        NEW_VERSION = '1.3.0'
        SERVER_CREDENTIALS = credentials('gitlab')
    }

    tools {
        maven "Maven-3.9.3"
        gradle "Gradle-6.2"
    }

    parameters {
        string(name: 'PROJECT NAME', defaultValue: '', description: 'name of the project')
        choice(name: 'VERSION', choices: ['1.1.0', '1.2.0', '1.3.0'], description: '')
        booleanParam(name: 'executeTests', defaultValue: true, description: '')
        // note that you cannot mix named and unamed arguments in the above. i.e string(name: 'jack', 'name of the project')
    }

    stages {
        stage("init") {
            steps {
                script {
                    gv = load "script.groovy"
                }
            }
        }

        stage("Run Frontend") {
            steps {
                script {
                    gv.runFrontend()
                }
            }
        }

        stage("build") {
            // when {
            //     expression {
            //         BRANCH_NAME=='dev' && CODE_CHANGES==true
            //     }
            // }
            // environmental variable "CODE_CHANGES" has not been defined.

            when {
                expression {
                    params.executeTests == false
                }
            }

            steps {
                script {
                    gv.buildApp()                
                }
            }
        }

        stage("test") {
            when {
                expression {
                    BRANCH_NAME=='dev' || BRANCH_NAME=='master'

                    params.executeTests //when this is set to true or written as this it's going to execute the stage and its steps because the parameter is true by default.
                    // params.executeTests == true  // can also be written like this as stated above
                }
            }

            // when {
            //     expression {
            //        params.executeTests //when this is set to true or written as this it's going to execute the stage and its steps because the parameter is true by default.
            //         // params.executeTests == true  // can also be written like this as stated above
            //     }
            // }
            // A stage cannot have two when statements

            steps {
                script {
                    gv.testApp()                
                }
            }
        }

        stage("deploy") {
            steps {
                script {
                    gv.deployApp()
                }

                withCredentials([usernamePassword(credentialsId: 'gitlab', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                    // this is a demonstration and there's no use for the gitlab credential in this Jenkinsfile
                }
            }
        }
    }
}
