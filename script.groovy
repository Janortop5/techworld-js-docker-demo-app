def runFrontend() {
    sh 'cd ./app'

    nodejs('Node-10.17') {
        sh 'npm list -g'
    }
}

def buildApp() {
    echo 'Building the application...'
    echo "Building version ${NEW_VERSION}"
    echo 'Building version ${NEW_VERSION}' // would not work
}

def testApp() {
    echo 'Testing the application...'
}

def deployApp() {
    echo 'Deploying the application...'
    sh 'mvn -version'

    // withGradle() {
    //     sh "./gradlew -v" 
    // }
    // didn't work for some reason

    // echo "deploying version params.VERSION" 
    // Wrong format to write it in so this won't work
    echo "deploying version ${params.VERSION}"
}

return this